----各種コマンド----
■各種コマンドの実行（db2コマンドの実行前） 即戦力のDB2管理術 第2章 P42〜	
db2ilist	インスタンス一覧の取得
db2idrop	インスタンスの削除
db2start	インスタンスを起動する。
db2level	DB2のバージョン等を表示する
db2stop 	"インスタンスの起動を停止する。
管理者用DB2コマンドプロンプトにて実行可能。"

■インストールしたDB2サーバーに付属しているClientを使って、DB2を動かしてみる。
・db2コマンドを省略できるようになる。
db2

・データベースの一覧を確認する。
db2 "LIST DB DIRECTORY"

・データベースに接続中のクライアント一覧を表示する。
db2 "LIST APPLICATIONS"

・管理者権限でのデータベースの接続。SAMPLEはデータベース名。
db2 connect to SAMPLE user db2admin using db2pass

・データベースの切断
db2 connect reset

・サンプル create table
db2 "create table T1 (ID int, NAME VARCHAR(10))"

・データベースを作成する。

デフォルトでは、
文字コード：UTF-8
ページ種別：IDENTITY
ページサイズ：8K
即戦力のDB2管理術 P61
db2 "create DATABASE データベース名

・CREATE DATABASE時の設定
即戦力のDB2管理術 P68,64
ストレージパス：テーブル設定情報の格納場所 P67

データベースパス：テーブルのデータそのものの格納場所 P67

db2 "create DATABASE データベース名
ON ストレージパス DBPATH ON データベースパス
USING CODESET コードセット TERRITORY JP
COLLATE USING ソート種別
PAGESIZE ページサイズ"


例

db2 "create DATABASE SAMPLE
ON C:\db2data DBPATH ON C:
USING CODESET UTF-8 TERRITORY JP
COLLATE USING SYSTEM_943_JP
PAGESIZE 4 K"



・サンプル insert文
db2 "insert into T1 values (1, 'ABC'),(2,'DEF')"

・サンプル select文
db2 "select * from t1"

・db2コマンドの終了
quit

・表の一覧を確認する.
db2 LIST TABLES

・テーブル定義を出力する。
db2 DESCRIBE TABLE テーブル名

・ユーザーが定義した索引の一覧を確認する例
DEFINER=''db2admin"はユーザ名
索引の一覧を確認するにはカタログ情報を直接参照します。 索引のカタログ情報はSYSCAT.INDEXESに記録されています。

db2 "SELECT INDSCHEMA, INDNAME, TABSCHEMA, TABNAME FROM SYSCAT.INDEXES WHERE DEFINER='DB2ADMIN'"
db2 "SELECT INDNAME, TABNAME FROM SYSCAT.INDEXES WHERE DEFINER='DB2ADMIN'"

・制約の一覧を確認する。

db2 "SELECT TABSCHEMA, TABNAME, CONSTNAME FROM SYSCAT.TABCONST WHERE TABSCHEMA='DB2INST1' AND TABNAME='EMPLOYEE'"
db2 "SELECT TABNAME, CONSTNAME FROM SYSCAT.TABCONST

・現在のスキーマ名を表示する。
スキーマを指定しない場合、SQLはこのカレントスキーマに適用される。

db2 "VALUES CURRENT SCHEMA"

・データベース構成パラメータ(DB CFG)
について、データベースの設定情報を確認する。（即戦力のDB2管理術 P69 パラメータには3種類有る。）
例えば、文字コード(UTF-8)、ページサイズ、文字の並び順（照合順序）
これらは、CREATE DATABASE時に指定する。

db2 "GET DATABASE CONFIGURATION FOR SAMPLE"
db2 "GET DB CFG FOR SAMPLE"
※SAMPLEはDB名

あるいは、
db2 "SELECT * FROM SYSIBMADM.DBCFG"

・データベースマネージャー構成パラメータ(DBM CFG)
について、データベースの設定情報を確認する。（即戦力のDB2管理術 P69 パラメータには3種類有る。）

db2 "GET DATABASE MANAGER CONFIGURATION"
db2 "GET DBM CFG"
※SAMPLEはDB名

あるいは、
db2 "SELECT * FROM SYSIBMADM.DBMCFG"

・レジストリー変数
について、データベースの設定情報を確認する。
これは、OS固有の設定値のことである。（即戦力のDB2管理術 P69 パラメータには3種類有る。）

db2set -all

あるいは、
db2 "SELECT * FROM SYSIBMADM.REG_VARIABLES"

・インスタンス（1つのデータベース）にレジストリ変数を設定する。即戦力のDB2管理術 P74
インスタンスレベル： db2set　変数名=値 
グローバルレベル： db2set　-g 変数名=値

■サンプルDDLダウンロード　即戦力のDB2管理術 P47	
http://gihyo.jp/book/2011/978-4-7741-4597-6/support

■db2コマンドを実行する前に以下をコマンド（サンプルDDLの実行）		
・DDLの読込と実行
db2 -tvf ./testdata/create_tables.sql		
・csvのインポート
db2 "IMPORT FROM ./testdata/member.csv OF DEL REPLACE INTO MEMBER"

db2 "IMPORT FROM ./testdata/division.csv OF DEL REPLACE INTO DIVISION"
・上記csvのインポート
db2 -tvf ./testdata/import.sql
・"インデックスやチェック制約、外部キー制約等を作成
（インポートする）"
db2 -tvf ./testdata/constraints.sql

■DB2　参考サイト
http://blue-red.ddo.jp/~ao/wiki/wiki.cgi?page=DB2+%C1%E0%BA%EE%A5%B3%A5%DE%A5%F3%A5%C9#p5
http://db2watch.com/wiki/index.php?title=%E3%83%A1%E3%82%A4%E3%83%B3%E3%83%9A%E3%83%BC%E3%82%B8

■DBViewer等のツールを使った方が良い。
なんせ、例えば、select文の結果で、各項目の幅が広すぎてみえづらい。。

----インストール---------------------------------------------------
■無料版 DB2 Express-Cをインストールする。
即戦力のDB2管理術 P414

■管理ユーザ名とポート番号
	1. DB2 の管理ユーザー名 (デフォルトでは、db2admin)
	2. 作成するインスタンスの TCP/IP ポート番号 (デフォルトでは、50000 番)
	
	DB2 サーバーは、Windows 上のサービスとして起動します。その起動には、上記の「DB2 の管理ユーザー」の ID とパスワードが使用されます。
	
■パスワードの設定
	
	インストールの途中でパスワードを設定する。
	例．
	ユーザー名：db2admin
	パスワード：db2pass

■マニュアル
	http://www-01.ibm.com/support/knowledgecenter/SSEPGG_10.5.0/com.ibm.db2.luw.kc.doc/welcome.html?lang=en
	https://www-947.ibm.com/support/entry/portal/documentation_expanded_list/information_management/db2_for_linux,_unix_and_windows?productContext=1690484112


----JDBCドライバ設定---------------------------------------------
■ローカルのjdbcドライバをEclipseのクラスパスに追加する。
	
	Project->Properties->JavaBuildPath->Libraries
	に以下のjarを追加
	C:\Program Files\IBM\SQLLIB\java\db2jcc4.jar
	C:\Program Files\IBM\SQLLIB\java\db2jcc_license_cu.jar

■applicatin.ymlに以下設定が必要
spring:
  datasource:
    ##H2database P52 H2データベースを使う設定
    #driverClassName: org.h2.Driver
    #url: jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
    #username: ca
    #password: 
    ##MySQL virtualBox内のMySQLに接続
    #driverClassName: com.mysql.jdbc.Driver
    #url: jdbc:mysql://192.168.33.10:3306/SpringBootSampleDB?useUnicode=true&characterEncoding=utf8
    #username: vagrant
    #password: vagrant
    ##DB2 localにインストールしたDB2に接続
    driverClassName: com.ibm.db2.jcc.DB2Driver
    url: jdbc:db2://localhost:50000/SAMPLE:currentSchema=DB2ADMIN; #jdbc:db2://<host>:<port>/<database>:currentSchema=<SchemaName>;
    username: db2admin
    password: db2pass
  jpa:
    hibernate.ddl-auto: create-drop   # H2の場合デフォルトでcreate-dropになっている様子。
    show-sql: true
    ##MySQL
    #database-platform: org.hibernate.dialect.MySQL5Dialect
    #database: MYSQL
    ##DB2
    database-platform: org.hibernate.dialect.DB2Dialect
    database: DB2
  view:
    prefix: /WEB-INF/views/
    suffix: .jsp

