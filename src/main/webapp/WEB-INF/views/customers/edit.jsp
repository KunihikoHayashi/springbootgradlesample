<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="function" uri="http://java.sun.com/jsp/jstl/functions"%>
<%--@ taglib prefix="xml" uri="http://java.sun.com/jsp/jstl/xml"--%>
<%--@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"--%>
<%--(参考)http://struts.wasureppoi.com/jstl/01_use.html--%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="../layout/layout.jsp">
  <jsp:param name="title" value="顧客一覧" /> 
  <jsp:param name="h1" value="顧客管理システム" /> 
  <jsp:param name="content">
  <jsp:attribute name="value">
  <div class="col-sm-12">

    <form:form modelAttribute="customerForm" method="post" action="/customers/edit" class="form-horizontal">
        <fieldset>
            <legend>顧客情報編集</legend>
            <spring:bind path="lastName">
            <div class="form-group ${status.isError() ? 'has-error has-feedback' : ''}">
                <form:label path="lastName" class="col-sm-2 control-label">姓</form:label>
                <div class="col-sm-10">
                    <form:input path="lastName" class="form-control" />
                    <form:errors path="lastName" cssStyle="color:red" element="span" class="help-block"/>
                </div>
            </div>
            </spring:bind>
            <spring:bind path="firstName">
            <div class="form-group ${status.isError() ? 'has-error has-feedback' : ''}">
                <form:label path="firstName" class="col-sm-2 control-label">名</form:label>
                <div class="col-sm-10">
                    <form:input path="firstName" class="form-control" />
                    <form:errors path="firstName" cssStyle="color:red" element="span" class="help-block"/>
                </div>
            </div>
            </spring:bind>
            <spring:bind path="gender">
            <div class="form-group ${status.isError() ? 'has-error has-feedback' : ''}">
                <form:label path="gender" class="col-sm-2 control-label">性別</form:label>
                <div class="col-sm-10">
                    <c:forEach var="item" items="${genderList}">
                    <label class="radio-inline"><form:radiobutton path="gender" class="radio-control" label="${item.value}" value="${item.name()}"/></label>
                    </c:forEach>
                    <form:errors path="gender" cssStyle="color:red" element="span" class="help-block"/>
                </div>
            </div>
            </spring:bind>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="submit" name="goToTop" value="戻る" class="btn btn-defalut" />
                    <input type="hidden" name="id" value="${param.id}" />
                    <input type="submit" value="更新" class="btn btn-primary" />
                </div>
            </div>
        </fieldset>
    </form:form>
  </div>
  </jsp:attribute>
  </jsp:param>
</jsp:include>
