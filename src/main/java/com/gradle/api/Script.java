package com.gradle.api;

import lombok.Data;

@Data
public class Script {

	public Script(String id, String name, Script script) {
        super();
        this.id = id;
        this.name = name;
        this.script = script;
    }

//    public Script(String id, String name) {
//        super();
//        this.id = id;
//        this.name = name;
//    }

    private String id;

    private String name;

  private Script script;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Script getScript() {
        return script;
    }

    public void setScript(Script script) {
        this.script = script;
    }

}
