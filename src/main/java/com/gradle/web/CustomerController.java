package com.gradle.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gradle.constant.Gender;
import com.gradle.domain.Customer;
import com.gradle.service.CustomerService;

import java.util.List;

@Controller
@RequestMapping("customers")
public class CustomerController {
	@Autowired
	CustomerService customerService;

	//P97 フォームの初期化で、@RequestMappingのあるメソッド実行前に実行される。
	//返り値は自動でModelクラスに設定される。
	@ModelAttribute
	CustomerForm setUpForm(){
		return new CustomerForm();
	}

	@ModelAttribute
	Gender[] setUpGenderEnum(){
	    return Gender.values();
	}
	
	//P93
	//Spring MVCでは画面に渡す値をModelインターフェースで渡す
	//渡し方はmodel.addAttribute
	//curl http://localhost:8080/customers -v -X GET
	@RequestMapping(method = RequestMethod.GET)
	String list(Model model) {
		List<Customer> customers = customerService.findAll();
		model.addAttribute("customers", customers);
		return "customers/list";
	}	

	@RequestMapping(value="create", method = RequestMethod.POST)
	String create(@Validated CustomerForm form, BindingResult result , Model model) {
		if(result.hasErrors()){
			return list(model);
		}
		Customer customer = new Customer();
		BeanUtils.copyProperties(form, customer);
		customerService.create(customer);
		return "redirect:/customers";
	}

	@RequestMapping(value="edit", params = "form" , method = RequestMethod.GET)
	String editForm(@RequestParam Integer id, CustomerForm form) {
		Customer customer = customerService.findOne(id);
		BeanUtils.copyProperties(customer,form);
		return "customers/edit";
	}

	@RequestMapping(value="edit", method = RequestMethod.POST)
	String edit(@RequestParam Integer id, @Validated CustomerForm form, BindingResult result) {
		if(result.hasErrors()){
			return editForm(id,form);
		}
		Customer customer = new Customer();
		BeanUtils.copyProperties(form,customer);
		customer.setId(id);
		customerService.update(customer);
		return "redirect:/customers";
	}

	@RequestMapping(value="edit", params = "goToTop")
	String goToTop() {
		return "redirect:/customers";
	}

	@RequestMapping(value="delete", method = RequestMethod.POST)
	String delete(@RequestParam Integer id) {
		customerService.delete(id);
		return "redirect:/customers";
	}

}
