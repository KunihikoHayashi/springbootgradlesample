package com.gradle.conf;

import net.sf.log4jdbc.Log4jdbcProxyDataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.web.filter.CharacterEncodingFilter;

import com.gradle.App;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.Filter;
import javax.sql.DataSource;

@Configuration
public class AppConfig {

	//P54
	@Autowired
	DataSourceProperties dataSourceProperties;
	DataSource dataSource;
	
	@Bean
	DataSource realDataSource() {
		DataSourceBuilder factory = DataSourceBuilder
				.create(this.dataSourceProperties.getClassLoader())
				.url(this.dataSourceProperties.getUrl())
				.username(this.dataSourceProperties.getUsername())
				.password(this.dataSourceProperties.getPassword());
		this.dataSource = factory.build();
		return this.dataSource;
	}
 
//    @Bean
//    public EntityManagerFactory entityManagerFactory(JpaVendorAdapter jpaVendorAdapter, DataSource dataSource) {
//        LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
//        bean.setDataSource(dataSource);
//        bean.setJpaVendorAdapter(jpaVendorAdapter);
//        bean.setPackagesToScan("com.gradle.domain");
//        bean.afterPropertiesSet();
//        return bean.getObject();
//    }

    @Bean
    public EntityManagerFactory entityManagerFactory() {
        HibernateJpaVendorAdapter vendorAdapter = 
            new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(true);
        vendorAdapter.setDatabasePlatform("DB2");
 
        LocalContainerEntityManagerFactoryBean factory = 
            new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan("com.gradle.domain");
        factory.setDataSource(realDataSource());
        factory.afterPropertiesSet();
        return factory.getObject();
    }

//    @Bean
//    public EntityManager entityManager(EntityManagerFactory entityManagerFactory) {
//        return entityManagerFactory.createEntityManager();
//    }

//	@Bean
//	public LocalContainerEntityManagerFactoryBean customerEntityManagerFactory(
//	        EntityManagerFactoryBuilder builder) {
//	    return builder
//	            .dataSource(dataSource)
//	            .packages(App.class)
//                .persistenceUnit("default")
//	            .build();
//	}

	//http://qiita.com/tag1216/items/76b8c1087d6b4b83550b
	//http://www.baeldung.com/2011/12/13/the-persistence-layer-with-spring-3-1-and-jpa/
	//http://blog.netgloo.com/2014/10/06/spring-boot-data-access-with-jpa-hibernate-and-mysql/
	//http://www.petrikainulainen.net/programming/spring-framework/spring-data-jpa-tutorial-part-one-configuration/
	//http://www.stefanalexandru.com/java/spring-4-with-jpa-hibernate-entitymanager-and-customrepositories
	//http://tono-n-chi.com/blog/2015/01/spring-boot-multiple-resource-connection-closed/
    //http://libro.tuyano.com/index3?id=7626003&page=7
//	@Bean
//	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
//	  LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
//	  em.setDataSource(this.realDataSource());
//	  em.setPackagesToScan(new String[]{"com.gradle.domain"}); 
//	  JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
//	  em.setJpaVendorAdapter(vendorAdapter);
//	  return em;
//	}

//	@Bean
//	DataSource dataSource() {
//		return new Log4jdbcProxyDataSource(this.dataSource);
//	}

	//P103 フォームから日本語を送信するために必要な設定
	//(参考)https://github.com/spring-projects/spring-boot/issues/540#issuecomment-45100334
	@Order(Ordered.HIGHEST_PRECEDENCE)
	@Bean
	Filter characterEncodingFilter() {
		CharacterEncodingFilter filter = new CharacterEncodingFilter();
		filter.setEncoding("UTF-8");
		filter.setForceEncoding(true);
		return filter;
	}

}
