package com.gradle.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gradle.domain.User;

public interface UserRepository extends JpaRepository<User,Integer> {
}
