package com.gradle.domain;

import javax.persistence.*;

//import org.springframework.data.annotation.CreatedBy;
//import org.springframework.data.annotation.CreatedDate;
//import org.springframework.data.annotation.LastModifiedBy;
//import org.springframework.data.annotation.LastModifiedDate;

//SpringData Auditing
//http://docs.spring.io/spring-data/jpa/docs/current/reference/html/#auditing

//JPA全般
//https://terasolunaorg.github.io/guideline/public_review/ArchitectureInDetail/DataAccessJpa.html#query

//http://etc9.hatenablog.com/entry/20090830/1251606823
@MappedSuperclass //BaseEnttityが永続化するのに必要
//public abstract class BaseEntity extends AbstractAuditable<User,Integer> {
//@EntityListeners(AuditingEntityListener.class)
public abstract class BaseEntity  {
    //@Autowired UserRepository userRepository;
    private static final Integer CREATED_USR_ID = 0;
    private static final Integer LAST_MODIFIED_USER_ID = 1;

    //楽観ロック
    //https://terasolunaorg.github.io/guideline/public_review/ArchitectureInDetail/ExclusionControl.html#jpa-spring-data-jpa
    @Version
    @Column(name = "version",nullable = false)
    private int version;

    //@ManyToOne
    //@CreatedBy
    @JoinColumn(name = "created_user_id",nullable = true)
    private Integer createdUserId;
    //private User createdUser;

    //Java8 DateTime http://www.atmarkit.co.jp/ait/articles/1412/16/news041.html
    //Java7まで java.util.Date java.util.Calendar
    //@CreatedDate
    //private Date createdDate;

    //@ManyToOne
    //@LastModifiedBy
    @JoinColumn(name = "last_modified_user_id",nullable = true)
    private Integer lastModifiedUserId;
    //private User lastModifiedUser;

    //@LastModifiedDate
    //private Date lastModifiedDate;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Integer getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Integer createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Integer getLastModifiedUserId() {
        return lastModifiedUserId;
    }

    public void setLastModifiedUserId(Integer lastModifiedUserId) {
        this.lastModifiedUserId = lastModifiedUserId;
    }

    

    // 変更時の自動設定
    //http://enterprisegeeks.hatenablog.com/entry/2014/12/24/083000
//    @PrePersist
//    private void prePersist(){
//        //setCreatedUser(userRepository.findOne(CREATED_USR_ID));
//        //setUpdated(new Date());
//    }

//    @PreUpdate
//    private void preUpdate() {
//        //setLastModifiedUser(userRepository.findOne(LAST_MODIFIED_USER_ID));
//    }
}
