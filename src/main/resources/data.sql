/* P50 SpringBootではクラスパス直下にdata.sql,schema.sql等のある名前のsqlファイルを置くと初期データとして読み込んで実行される*/ 
INSERT INTO customers(id, firstname,lastname,gender,version,createduserid,lastmodifieduserid) VALUES(1, 'Nobita','Nobi',0,0,0,0);
INSERT INTO customers(id, firstname,lastname,gender,version,createduserid,lastmodifieduserid) VALUES(2, 'Takeshi','Goda',0,0,0,0);
INSERT INTO customers(id, firstname,lastname,gender,version,createduserid,lastmodifieduserid) VALUES(3, 'Suneo','Honekawa',0,0,0,0);
INSERT INTO customers(id, firstname,lastname,gender,version,createduserid,lastmodifieduserid) VALUES(4, 'Shizuka','Minamoto',1,0,0,0);
